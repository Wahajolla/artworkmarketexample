﻿using System;
using Microsoft.EntityFrameworkCore;
using SharedData;

namespace DataModel
{
    public class ArtMarketContext : DbContext
    {
        
        
        public DbSet<ArtWork> ArtWorks { get; set; }
        public DbSet<Order> ArtOrders { get; set; }
        public DbSet<User> Users { get; set; }
    }
    
    
}