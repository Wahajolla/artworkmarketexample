﻿using Microsoft.AspNetCore.Mvc;

namespace Orders.API.Controllers
{
   
    public class HomeController : Controller
    {
        [Route("")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public RedirectResult RedirectToSwagger()
        {
            return new RedirectResult("/swagger/");
        }
    }
}