﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SharedData;
using Model;

namespace Orders.API.Controllers
{
    [Authorize] 
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
       // private readonly DataContext _ordersContext;

        /*public OrdersContoller(DataContext ordersContext)
        {
            _ordersContext = ordersContext;
        }*/
        
        /// <summary>
        /// Получает заказ по Id.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Возвращает последнюю картину</returns>
        /// <response code="201">Возвращает картину</response>
        /// <response code="400">Если картины нет</response>
        [HttpGet]
        public ActionResult<Order> Get()
        {

            return new Order() {Id = 1};
        }
        
        
        
        /// <summary>
        /// Создает/получает заказ по Id.
        /// </summary>
        /// <param name="item"></param>
        [HttpPost]
        [ProducesResponseType(typeof(Order), (int)HttpStatusCode.OK)]
        public ActionResult<Order> UpdateOrder([FromBody] Order newOrder,IFormFile file)
        {
           

            //ТУТ БУДЕТ ДОБАВЛЕНИЕ/ОБНОВЛЕНИЕ
            var rng = new Random();
            return new Order(){Id = 2};
        }
        
        
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        public ActionResult<Order> DeleteOrderByAsync(string id)
        {
            //ТУТ БУДЕТ УДАЛЕНИЕ
            var rng = new Random();
            return new Order(){Id = 2};
        }
    }
}