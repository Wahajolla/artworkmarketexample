﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SharedData
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public byte[] FileToManage { get; set; }
        public ArtWork Art { get; set; }
        public string Description { get; set; }
        public ArtType Type { get; set; }
        public float Price { get; set; }
        public User Author { get; set; }
        public User Buyer { get; set; }
    }
}