﻿namespace SharedData
{
    public class ArtWork
    {
        public int Id { get; set; }
        public byte[] FileToManage { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public ArtType ArtType { get; set; }
        public User Author { get; set; }
    }
}