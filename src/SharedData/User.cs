﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SharedData
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        
        public UserType Type { get; set; }
        
        public string Login { get; set; }
        public string ShownLogin { get; set; }
        
        [PasswordPropertyText]
        public string Password { get; set; }
        
        
        public int[] OrderId { get; set; }  
    }
}