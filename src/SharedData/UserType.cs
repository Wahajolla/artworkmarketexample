﻿namespace SharedData
{
    public enum UserType
    {
        User,
        Artist,
        Admin
    }
}