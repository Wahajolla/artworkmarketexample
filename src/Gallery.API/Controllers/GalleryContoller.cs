﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SharedData;

namespace ArtWorkGallery.API.Controllers
{
    [ApiController]
    [Route("gallery")]
    [Produces("application/json")]
    public class GalleryContoller : ControllerBase
    {
     

        private readonly ILogger<GalleryContoller> _logger;
        private readonly IWebHostEnvironment _env;

        public string _imagesPath;

        public GalleryContoller(ILogger<GalleryContoller> logger,IWebHostEnvironment  env)
        {
            _logger = logger;
            _env = env;
        }
        
        /// <summary>
        /// Получает картину по Id.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Возвращает последнюю картину</returns>
        /// <response code="201">Возвращает картину</response>
        /// <response code="400">Если картины нет</response>
        [HttpGet("{id}")]
        public ActionResult<ArtWork> Get(int id)
        {
            var rng = new Random();
            return new ArtWork(){Id = 2};
        }
        
        /// <summary>
        /// Создает/получает работу по Id.
        /// </summary>
        /// <param name="item"></param>
        [HttpPost]
        [ProducesResponseType(typeof(Order), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Order>> UpdateGallery(int id, IFormFile file)
        {

            return new Order();
        }
    }
}