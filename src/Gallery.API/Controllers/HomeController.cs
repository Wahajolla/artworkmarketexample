﻿using Microsoft.AspNetCore.Mvc;

namespace ArtWorkGallery.API.Controllers
{
    [Route("home")]
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }
    }
}