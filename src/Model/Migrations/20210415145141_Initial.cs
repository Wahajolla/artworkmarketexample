﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ArtWorks",
                table: "ArtWorks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArtOrders",
                table: "ArtOrders");

            migrationBuilder.RenameTable(
                name: "ArtWorks",
                newName: "Works");

            migrationBuilder.RenameTable(
                name: "ArtOrders",
                newName: "Orders");

            migrationBuilder.RenameColumn(
                name: "ArtWorkId",
                table: "Orders",
                newName: "BuyerId");

            migrationBuilder.RenameColumn(
                name: "ArtType",
                table: "Orders",
                newName: "Type");

            migrationBuilder.AddColumn<int>(
                name: "ArtId",
                table: "Orders",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "Orders",
                type: "integer",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Works",
                table: "Works",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Orders",
                table: "Orders",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 4,
                column: "BuyerId",
                value: null);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ArtId",
                table: "Orders",
                column: "ArtId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_AuthorId",
                table: "Orders",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BuyerId",
                table: "Orders",
                column: "BuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_AuthorId",
                table: "Orders",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_BuyerId",
                table: "Orders",
                column: "BuyerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Works_ArtId",
                table: "Orders",
                column: "ArtId",
                principalTable: "Works",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_AuthorId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_BuyerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Works_ArtId",
                table: "Orders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Works",
                table: "Works");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Orders",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ArtId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_AuthorId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_BuyerId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ArtId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Orders");

            migrationBuilder.RenameTable(
                name: "Works",
                newName: "ArtWorks");

            migrationBuilder.RenameTable(
                name: "Orders",
                newName: "ArtOrders");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "ArtOrders",
                newName: "ArtType");

            migrationBuilder.RenameColumn(
                name: "BuyerId",
                table: "ArtOrders",
                newName: "ArtWorkId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArtWorks",
                table: "ArtWorks",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArtOrders",
                table: "ArtOrders",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "ArtOrders",
                keyColumn: "Id",
                keyValue: 4,
                column: "ArtWorkId",
                value: 1);
        }
    }
}
