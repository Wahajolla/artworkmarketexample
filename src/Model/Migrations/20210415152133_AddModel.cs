﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class AddModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "Works",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Works_AuthorId",
                table: "Works",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Works_Users_AuthorId",
                table: "Works",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Works_Users_AuthorId",
                table: "Works");

            migrationBuilder.DropIndex(
                name: "IX_Works_AuthorId",
                table: "Works");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Works");
        }
    }
}
