﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SharedData;

namespace Model
{
    public class DataContext : DbContext
    {
        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=artMarket;User ID=marketBack;Password=11111;Pooling=true;commandtimeout=0");
            base.OnConfiguring(optionsBuilder);
        }

        public DataContext(DbContextOptionsBuilder optionsBuilder)
        {
            Database.EnsureCreated();
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().HasData(new List<Order>()
            {
                new Order() {Id = 1},
                new Order(){Id = 2},
                new Order(){Id = 3},
                new Order(){Id = 4},
            });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<ArtWork> Works { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }
    }
}